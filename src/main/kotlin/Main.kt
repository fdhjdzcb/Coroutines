import java.net.HttpURLConnection
import java.net.URL
import java.util.Scanner
import kotlinx.coroutines.*

fun main() = runBlocking {
    while (true) {
        val scanner = Scanner(System.`in`)
        print("Введите список сайтов через пробел: ")
        val websites = scanner.nextLine().split(" ")

        val results = async {
            websites.map { website ->
                val isAvailable = checkWebsite(website)
                "Сайт $website ${if (isAvailable) "доступен" else "недоступен"}"
            }
        }

        results.await().forEach { println(it) }
    }
}

suspend fun checkWebsite(url: String): Boolean {
    return try {
        val connection = URL(url).openConnection() as HttpURLConnection
        connection.requestMethod = "HEAD"
        connection.connectTimeout = 5000
        connection.readTimeout = 5000
        connection.responseCode == HttpURLConnection.HTTP_OK
    } catch (e: Exception) {
        false
    }
}